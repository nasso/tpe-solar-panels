// - GLOBALS -
// elements
const graphCvs = get_by_id("graph-cvs");
const refreshbtn = get_by_id("refresh-btn");
const statusCell = {
	power: get_by_id("output-power-cell"),
	light: get_by_id("light-value-cell")
};
let modeSwitch = {};
modeSwitch[MODE.AUTO] = get_by_id("mode-switch-auto");
modeSwitch[MODE.SEMI] = get_by_id("mode-switch-semi");
modeSwitch[MODE.OFF] = get_by_id("mode-switch-off");
let modePanel = {};
modePanel[MODE.AUTO] = get_by_id("mode-panel-auto");
modePanel[MODE.SEMI] = get_by_id("mode-panel-semi");
modePanel[MODE.OFF] = get_by_id("mode-panel-off");

// others
const graphCtx = graphCvs.getContext("2d");
let currentMode = null;

// - FUNCTIONS -
function redrawGraph() {
	const cvs = graphCvs;
	const gtx = graphCtx;
	const w = cvs.clientWidth;
	const h = cvs.clientHeight;
	const lw = 2;
	const gridSize = 6;
	const scaleY = {
		"power": 5,
		"light": 100000
	};
	const graphStyle = {
		"power": "red",
		"light": "yellow"
	};

	Remote.getDayData((e) => {
		const d = e.data;

		if(cvs.width !== w) cvs.width = w;
		if(cvs.height !== h) cvs.height = h;

		gtx.save();
		gtx.clearRect(0, 0, w, h);

		gtx.strokeStyle = "#333";
		gtx.lineWidth = 1;
		gtx.beginPath();
		for(let i = 1; i < gridSize; i++) {
			// X
			gtx.moveTo(
				lw / 2 + (w / (gridSize - 1)) * i,
				lw / 2
			);
			gtx.lineTo(
				lw / 2 + (w / (gridSize - 1)) * i,
				h - lw / 2
			);

			// Y
			gtx.moveTo(
				lw / 2,
				lw / 2 + (h / (gridSize - 1)) * i
			);
			gtx.lineTo(
				w - lw / 2,
				lw / 2 + (h / (gridSize - 1)) * i
			);
		}
		gtx.rect(lw / 2, lw / 2, w - lw, h - lw);
		gtx.stroke();

		gtx.strokeStyle = "royalblue";
		gtx.lineCap = "round";
		gtx.lineJoin = "round";
		gtx.lineWidth = lw;

		/*
		( +1 +0 +0 )
		( +0 -1 +h )
		( +0 +0 +1 )
		*/
		gtx.transform(1, 0, 0, -1, 0, h);

		for(let graphName in d) {
			const graphD = d[graphName];

			gtx.beginPath();
			for(let i = 0; i < graphD.length; i++) {
				gtx[i === 0 ? "moveTo" : "lineTo"](lw / 2 + i / (graphD.length - 1) * (w - lw / 2), graphD[i] / scaleY[graphName] * (h - lw) + lw / 2);
			}

			gtx.strokeStyle = graphStyle[graphName];
			gtx.stroke();
		}

		gtx.restore();
	});
}

function switchToMode(mode) {
	if(currentMode !== null) {
		modeSwitch[currentMode].classList.toggle("selected", false);
		modePanel[currentMode].classList.toggle("selected", false);
	}

	Remote.setCurrentMode(mode);
	currentMode = mode;
	modeSwitch[currentMode].classList.toggle("selected", true);
	modePanel[currentMode].classList.toggle("selected", true);
}

function notifyStatusChanges(power, light) {
	statusCell.power.innerHTML = power;
	statusCell.light.innerHTML = decimalRound(light, 3);
	redrawGraph();
}

function refreshStatus() {
	Remote.getStatusUpdate(function(e) {
		if(e.status === 200) {
			notifyStatusChanges(e.data.power, e.data.light);
		}
	});

	setTimeout(refreshStatus, CLIENT_STATUS_REFRESH_RATE);
}

// - MAIN -
// mode
for(let x in MODE) {
	const THIS_MODE_NAME = x;
	const THIS_MODE = MODE[THIS_MODE_NAME];

	modeSwitch[THIS_MODE].addEventListener("click", (e) => {
		switchToMode(THIS_MODE);
	});
}

window.addEventListener("keydown", (e) => {
	switch(e.keyCode) {
		case 37:
			switch(currentMode) {
				case MODE.AUTO:
					switchToMode(MODE.OFF);
					break;
				case MODE.SEMI:
					switchToMode(MODE.AUTO);
					break;
				case MODE.OFF:
					switchToMode(MODE.SEMI);
					break;
			}
			break;
		case 39:
			switch(currentMode) {
				case MODE.AUTO:
					switchToMode(MODE.SEMI);
					break;
				case MODE.SEMI:
					switchToMode(MODE.OFF);
					break;
				case MODE.OFF:
					switchToMode(MODE.AUTO);
					break;
			}
			break;
	}
});

Remote.getCurrentMode((e) => {
	switchToMode(e.data);
});

refreshbtn.addEventListener("click", (e) => {
	Remote.forceStatusUpdate(function(e) {
		notifyStatusChanges(e.data.power, decimalRound(e.data.light, 3));
	});
});

// graph
redrawGraph();

// status
refreshStatus();
