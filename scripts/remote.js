let Remote = {
	// float[]{} getDayData();

	// int getCurrentMode();
	// void setCurrentMode(MODE mode);

	// float{} getStatusUpdate();
	// float forceStatusUpdate();

	// void clean();
};

// CALLBACKS:
/*
{
	status: HTTP_STATUS_CODE,
	(data: ANYTHING_REALLY),
	(error: ERROR_MESSAGE),
}
*/

const ONLINE = false;
const DEBUG_OUT = true;

function dbg(msg) {
	if(DEBUG_OUT) console.log(msg);
}

if(!ONLINE) {
	const VIRTUAL_UPDATE_TIME = 10; // time it takes for the light meter to update (default 6000)
	const STATUS_VALUES_REFRESH_RATE = 1000; // time between each new measure (light meter opens) (default 60k)
	const VIRTUAL_DAY_TIME = 10000;

	let fakeDayData = {
		power: new Array(48),
		light: new Array(48)
	};
	let lastDayUpdateTimeIndex = -1;
	let lightmetervalue = 0;
	let outputpower = 0;
	let statusRefreshTimeout = null;

	for(let i = 0; i < 48; i++) {
		fakeDayData.power[i] = 0;
		fakeDayData.light[i] = 0;
	}

	function virtualDayTime() {
		return (Date.now() % VIRTUAL_DAY_TIME) / VIRTUAL_DAY_TIME;
	}

	function pushNewData() {
		// 48 because 30 mins * 48 = 24 hours
		let currentTimeIndex = Math.floor(virtualDayTime() * 48);

		if(currentTimeIndex != lastDayUpdateTimeIndex);

		for(let i = 0; i < 47; i++) {
			fakeDayData.power[i] = fakeDayData.power[i + 1];
			fakeDayData.light[i] = fakeDayData.light[i + 1];
		}

		lastDayUpdateTimeIndex = currentTimeIndex;
		fakeDayData.power[47] = outputpower;
		fakeDayData.light[47] = lightmetervalue;
	}

	function refreshStatus(preventPush) {
		if(statusRefreshTimeout) clearTimeout(statusRefreshTimeout);

		lightmetervalue = Math.floor(lightOfDayApprox(virtualDayTime()) * 100000);
		outputpower = Math.max(decimalRound(lightmetervalue / 100000 * 4 + (Math.random() * 2 - 1) * 0.1, 2), 0);
		if(!preventPush) pushNewData();

		statusRefreshTimeout = setTimeout(refreshStatus, STATUS_VALUES_REFRESH_RATE);
	}

	{ // Remote::getDayData
		Remote.getDayData = function(callback) {
			dbg("getDayData");

			if(typeof(callback) === "function") callback({
				status: 200,
				data: fakeDayData
			});
		}
	}

	{ // Remote::currentMode accessors
		let fakeMode = MODE.AUTO;

		// Remote::getCurrentMode
		Remote.getCurrentMode = function(callback) {
			dbg("getCurrentMode");

			if(typeof(callback) === "function") callback({
				status: 200,
				data: fakeMode
			});
		}

		// Remote::setCurrentMode
		Remote.setCurrentMode = function(mode, callback) {
			dbg("setCurrentMode");

			if(mode !== MODE.AUTO && mode !== MODE.SEMI && mode !== MODE.OFF) {
				if(typeof(callback) === "function") callback({
					status: 400,
					error: "Unknown mode: " + mode
				});
			} else {
				fakeMode = mode;

				if(typeof(callback) === "function") callback({
					status: 200,
					data: mode
				});
			}
		}
	}

	{ // Remote::getStatusUpdate();
		Remote.getStatusUpdate = function(callback) {
			dbg("getStatusUpdate");

			if(typeof(callback) === "function") callback({
				status: 200,
				data: {
					light: lightmetervalue, // (simulate 1 day = 2 mins)
					power: outputpower
				}
			});
		};
	}

	{ // Remote:: forceStatusUpdate();
		Remote.forceStatusUpdate = function(callback) {
			dbg("forceStatusUpdate");

			refreshStatus(true);
			if(typeof(callback) === "function") setTimeout(function() {
				callback({
					status: 200,
					data: {
						power: outputpower,
						light: lightmetervalue
					}
				});
			}, VIRTUAL_UPDATE_TIME);
		};
	}

	{
		Remote.clean = function() {
			// TODO: Clean here
		}
	}

	refreshStatus();
} else {
	// TODO: Online interface
}
