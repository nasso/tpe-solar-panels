const MODE = Object.freeze({
	AUTO: Symbol("auto"),
	SEMI: Symbol("semi"),
	OFF: Symbol("off")
});

// Default: 5000
const CLIENT_STATUS_REFRESH_RATE = 5000; // time between each update (pull values from the server)
