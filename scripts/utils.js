function get_by_id(id) {
	return document.getElementById(id);
}

function clamp(x, min, max) {
	return Math.min(Math.max(x, min), max);
}

function lightOfDayApprox(time) {
	return clamp(0.5 * Math.sin(2 * Math.PI * time + 1.5 * Math.PI) + 0.5 + (Math.random() * 2 - 1) * 0.025, 0, 1);
}

function decimalRound(n, p) {
	let f = Math.pow(10, Math.floor(p));
	return Math.floor(n * f) / f;
}
